ALTER TABLE `motor` ADD `created_by` INT NULL DEFAULT NULL AFTER `support_docs`;
ALTER TABLE `motor` CHANGE `created_on` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `drugs` ADD `created_by` INT(50) NULL AFTER `other`;
ALTER TABLE `food` ADD `created_by` INT(50) NULL AFTER `other`

<?php

//Dashboard page
Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

//Motor imports page
Breadcrumbs::for('admin.motor.index', function ($trail) {
    $trail->push('Manage Motor Imports', route('admin.motor.index'));
});

Breadcrumbs::for('admin.motor.create', function ($trail) {
    $trail->push('Create Motor Imports', route('admin.motor.create'));
});

//Drugs imports page
Breadcrumbs::for('admin.drugs.index', function ($trail) {
    $trail->push('Manage Drugs Imports', route('admin.drugs.index'));
});

Breadcrumbs::for('admin.drugs.create', function ($trail) {
    $trail->push('Create Drugs Imports', route('admin.drugs.create'));
});

//Food imports page
Breadcrumbs::for('admin.food.index', function ($trail) {
    $trail->push('Manage Food Imports', route('admin.food.index'));
});

Breadcrumbs::for('admin.food.create', function ($trail) {
    $trail->push('Create Food Imports', route('admin.food.create'));
});

Breadcrumbs::for('admin.', function ($trail) {
    $trail->push('Title Here', route('admin.'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';

<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\MotorController;
use App\Http\Controllers\Backend\DrugsController;
use App\Http\Controllers\Backend\FoodController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('motor', [MotorController::class, 'index'])->name('motor.index');
Route::get('motor/create', [MotorController::class, 'create'])->name('motor.create');
Route::post('motor/import', [MotorController::class, 'import'])->name('motor.import');

Route::get('drugs', [DrugsController::class, 'index'])->name('drugs.index');
Route::get('drugs/create', [DrugsController::class, 'create'])->name('drugs.create');
Route::post('drugs/import', [DrugsController::class, 'import'])->name('drugs.import');

Route::get('food', [FoodController::class, 'index'])->name('food.index');
Route::get('food/create', [FoodController::class, 'create'])->name('food.create');
Route::post('food/import', [FoodController::class, 'import'])->name('food.import');
Route::view('recall/view', 'backend.sendAlert');
Route::get('recall/alert',[DashboardController::class,'recallAlert'])->name('recall.alert');
@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@push('after-styles')
<style>
    .multiselect {
        border: 1px solid lightgray;

    }
</style>
@endpush
@section('content')
<div class="row mb-4">
    <div class="col">
        @if(session('result') == "success")
        <div class="alert alert-success">
        {{session('msg')}}
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <i class="fas fa-home"></i> @lang('navs.general.home')
            </div>
            <div class="card-body">
                @lang('strings.frontend.welcome_to', ['place' => app_name()])
            </div>
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
<!--row-->

<div class="row mb-4">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Motor</h2>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="{{route('frontend.user.subscribe.view',['type'=>'motor'])}}" class="btn btn-primary">Subscribe</a>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Food</h2>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="{{route('frontend.user.subscribe.view',['type'=>'food'])}}" class="btn btn-primary">Subscribe</a>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Drugs</h2>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="{{route('frontend.user.subscribe.view',['type'=>'drugs'])}}" class="btn btn-primary">Subscribe</a>
            </div>
        </div>
    </div>
</div>
<!--row-->
@if(isset($type))
<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h3> {{$type}} <small>Subscription</small> </h3>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-md-12">

                        <form class="row" action="{{ (isset($edit) && $edit) ? route('frontend.user.subscribe.update',['type'=>$type]) :  route('frontend.user.subscribe.add',['type'=>$type])}}" method="POST">
                            @csrf
                            @foreach($subscriptionEntity as $fields)
                            @php 
                            $edit_values = "";
                            $input_value = implode(",",array_column($values, $fields->column_name));
                            if(isset($edit) && $edit){
                                $field_values = $filters->where('column_name',$fields->column_name)->first();

                                $edit_values =  $field_values->column_value ?? '';
                               
                            } 

                            @endphp


                            <div class="form-group col-md-3">
                                <label for="exampleFormControlInput1">{{$fields->display_name}}</label>
                                {!! render_html(
                                $fields->field_type,
                                $fields->column_name,
                                $input_value ,
                                $edit_values ,
                                $fields->attributes) !!}
                                <!-- <input type="email" name="{{$fields->column_name}}" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>
                            @endforeach

                            <div class="form-group col-md-12">
                                <input class="btn btn-success" type="submit" />
                            </div>
                        </form>
                    </div>



                </div>
            </div>
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
@endif

@if(isset($filters) && count($filters) > 0 )
<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h3> {{($type ?? '')}} <small>Subscription List</small> </h3>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-md-12">

                    @php
                    $food = $filters->where('category','food');
                    $motor = $filters->where('category','motor');
                    $drugs = $filters->where('category','drugs');
                    
                    @endphp

                    @if(count($motor))
                    <h2>Motor</h2>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Filter</th>
                                <th>Value</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($filters) && count($filters) > 0 )
                            @foreach($motor as $key=>$value)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$value->column_name}}</td>
                                <td>{{$value->column_value}}</td>
                                
                                <td><a href="{{route('frontend.user.subscribe.edit',['type'=>'motor'])}}" class="btn btn-success">Edit</a></td>
                            </tr>

                            @endforeach
                            @endif


                        </tbody>
                    </table>
                    @endif

                    @if(count($food))
                    <h2>Food</h2>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Filter</th>
                                <th>Value</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($filters) && count($filters) > 0 )
                            @foreach($food as $key=>$value)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$value->column_name}}</td>
                                <td>{{$value->column_value}}</td>
                                
                                <td><a href="{{route('frontend.user.subscribe.edit',['type'=>'food'])}}" class="btn btn-success">Edit</a></td>
                            </tr>

                            @endforeach
                            @endif


                        </tbody>
                    </table>
                    @endif

                    @if(count($drugs))
                    <h2>Drugs</h2>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Filter</th>
                                <th>Value</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($filters) && count($filters) > 0 )
                            @foreach($drugs as $key=>$value)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$value->column_name}}</td>
                                <td>{{$value->column_value}}</td>
                                
                                <td><a href="{{route('frontend.user.subscribe.edit',['type'=>'drugs'])}}" class="btn btn-success">Edit</a></td>
                            </tr>

                            @endforeach
                            @endif


                        </tbody>
                    </table>
                    @endif

                    </div>



                </div>
            </div>
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
@endif

<!--row-->
@endsection

@push('after-scripts')
<script>
    $("select[multiple]").multiselect();
</script>

@endpush
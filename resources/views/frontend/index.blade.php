<style>

</style>

@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@push('after-styles')
<style>
    .multiselect {
        border: 1px solid lightgray;
    }

    .multiselect-native-select>.btn-group {
        width: -webkit-fill-available;
    }

    .multiselect {
        width: -webkit-fill-available;
    }
</style>
@endpush
@section('content')
<div class=" mb-4">
    <div>
        @if(session('result') == "success")
        <div class="alert alert-success">
            {{session('msg')}}
        </div>
        @endif
        <div class="card index_card">
            <div class="card-body">
                <!-- Banner -->
                  <div class="container-fluid nopadding">
                <div class="banner_container">
                     <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-lg-6">
                            <div class="left_img">
                                <img src="../public/img/frontend/recall_home_img.png" alt="">   
                            </div>
                        </div>
                       
                        <div class="col-12 col-sm-12 col-lg-6">
                            <div class="right_content">
                                <h2>Online Recalls Manager</h2> 
                                <h5>About us</h5>
                                <p> RecallALARMS is a platform that brings you one step closer to an informed society; a platform   that can save lives from the negative impact of defective goods. The consumers, the retailers and the manufacturers are benefited by the amazing features offered by RecallALARMS. This application plays a vital role in protecting the consumers and in keeping the reputation of manufactures intact.  </p>

                                <p>  Retailers can perform better tracking of recalled products and contact affected customers within shorter time frames.  </p>

                            </div>
                        </div>
                        </div>
                    </div>
                </div>
               </div>
                <!-- Banner  -->



                <div class="content container">
                    <div class="col-sm-12">
                        <div class="product_info">
                            <h3> RecallALARMS: Recalling Products with Efficiency </h3>
                            <p> The negative effects of defective products in everyday life are fatal and the effect is long lasting. Recalling them is essential and doing so at the right time is critical. RecallALARMS is innovative technology at its best, bringing you easier and faster protection through immediate recall messages straight to the palm of your hand. We help both customers and businesses avoid the dangers of late recalls.  </p>

                            <h3> Track All Communications  </h3>
                            <p> One of the most pressing issues with recalls is the lack of communication that leads to many defective products going unnoticed or getting lost. With our application, communication capabilities are greatly increased, resulting in less lost recalls and reduced costs for your company. </p> 
                        </div>
                    </div>
                    <div class="col-sm-12">
                        
                         <div class="row ">
    <div class="col-sm-6 col-md-4">
      <div class="icon-ppt">
       <img src="../public/img/frontend/shopping.png">
       <span>Defective Product Purchased</span>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="icon-ppt">
      <img src="../public/img/frontend/alert.png">
      <span>Instant Recall Alert</span>
      </div>
   </div>
   <div class="col-sm-6 col-md-4">
    <div class="icon-ppt">
    <img src="../public/img/frontend/throwing-garbage.png">
    <span>Dispose of Fatal Product</span>
    </div>
 </div>
  </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="list-group-item list-group-item-action active"> For Consumers </h4>
                                <ul class="for_c">
                                    <li class="list-group-item list-group-item-action"> 	Consumers need not contact the  customer support team or  service centres to know about the defective products and related details. Registered users will receive an SMS and email notifications on the defective products instantly. This feature saves time and avoids unfixable mistakes for the consumer.  </li>
                                    <li class="list-group-item list-group-item-action"> The Consumer  can also view the history of recalled products using this app. This feature helps the Consumers by showing them which products to avoid and taking them one step closer to a safer future.  </li>
                                    <li class="list-group-item list-group-item-action"> Consumers can also view the defects of multiple products using this app. </li>
                                </ul>
                            </div>

                            <div class="col-sm-6">
                                <h4 class="list-group-item list-group-item-action active">For Manufacturers </h4>
                                <ul class="for_c">
                                    <li class="list-group-item list-group-item-action"> 	RecallALARMS is a boon to Manufacturers as it saves face, money and reputation.</li>
                                    <li class="list-group-item list-group-item-action"> 	The manufacturers can reach the Consumers through the retailers via the RecallALARMS application. </li>
                                    <li class="list-group-item list-group-item-action"> 	History of recalled products benefits Manufacturers to reduce recalls and save costs. </li>
                                    <li class="list-group-item list-group-item-action">	By knowing which products are recalled, manufacturers can work on rectifying the product or replacing them with a more profitable alternative.</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="recall-categorys">

                        <div class="col-sm-12">
                            <h3> Recall Categories </h3>

                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-3 mt-2">
                                    <div class="product-card">
                                        <img class="product-card-img-top" src="../public/img/frontend/motor-vehicle.jpg">
                                        <div class="product-card-block">
                                            <span class="text-bold">Automobile</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3 mt-2">
                                    <div class="product-card">
                                        <img class="product-card-img-top" src="../public/img/frontend/food.jpg">
                                        <div class="product-card-block">
                                            <span class="text-bold">Food / beverage</span>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-sm-6 col-md-4 col-lg-3 mt-2">
                                    <div class="product-card">
                                        <img class="product-card-img-top" src="../public/img/frontend/medicine.jpg">
                                        <div class="product-card-block">
                                            <span class="text-bold">Pharmaceutical</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3  mt-2">
                                    <div class="product-card">
                                        <img class="product-card-img-top" src="../public/img/frontend/child-safety-seat.jpg">
                                        <div class="product-card-block">
                                            <span class="text-bold">Child Safety</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="collaborators-card">  
                    <div class=" text-center"> 
                        <a  class="btn btn-primary" href="/register"> <i class="fa fa-bell" aria-hidden="true"></i> Subscribe </a>
                    </div> 
                </div>

                <div class="col-sm-12">
                    <div class="copyright">
                        <span> © Copyright 2020 Recallalarms, Inc. All rights reserved. </span>
                    </div>
                </div>


            </div>
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
<!--row-->
@if(isset($data))
<div class="row mb-4">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Auto mobile / Trucks</h2>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <!-- <a href="{{route('frontend.user.subscribe.view',['type'=>'motor'])}}" class="btn btn-primary">Subscribe</a> -->


                <HR>
                <div class="row mb-4">
                    <div class="col-md-12">

                        <form class="row" action="{{   route('frontend.user.subscribe.add',['type'=>'motor'])}}" method="POST">

                            @csrf

                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Make</label>
                                <select class="" name="make[]" multiple>
                                    @foreach(array_column($data['motor'],'make') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>

                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Model</label>
                                <select class="" name="model[]" multiple>
                                    @foreach(array_column($data['motor'],'model') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Year</label>
                                <select class="" name="year[]" multiple>
                                    @foreach(array_column($data['motor'],'year') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>

                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">VIN Number</label>
                                <select class="" name="vin[]" multiple>
                                    @foreach(array_column($data['motor'],'vin') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>



                            <div class="form-group col-md-12">
                                <input class="btn btn-primary" type="submit" value="Subscribe" />
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Food</h2>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <!-- <a href="{{route('frontend.user.subscribe.view',['type'=>'food'])}}" class="btn btn-primary">Subscribe</a> -->

                <HR>
                <div class="row mb-4">
                    <div class="col-md-12">

                        <form class="row" action="{{   route('frontend.user.subscribe.add',['type'=>'food'])}}" method="POST">

                            @csrf

                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Product Name</label>
                                <select class="" name="product_name[]" multiple>
                                    @foreach(array_column($data['food'],'product_name') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>

                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Unit Number</label>
                                <select class="" name="unit_number[]" multiple>
                                    @foreach(array_column($data['food'],'unit_number') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Remedy Type</label>
                                <select class="" name="remedy_type[]" multiple>
                                    @foreach(array_column($data['food'],'remedy_type') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>





                            <div class="form-group col-md-12">
                                <input class="btn btn-primary" type="submit" value="Subscribe" />
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Medication</h2>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <!-- <a href="{{route('frontend.user.subscribe.view',['type'=>'drugs'])}}" class="btn btn-primary">Subscribe</a> -->


                <HR>
                <div class="row mb-4">
                    <div class="col-md-12">

                        <form class="row" action="{{   route('frontend.user.subscribe.add',['type'=>'drugs'])}}" method="POST">

                            @csrf

                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Product Name</label>
                                <select class="" name="product_name[]" multiple>
                                    @foreach(array_column($data['drugs'],'product_name') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>

                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Unit Number</label>
                                <select class="" name="unit_number[]" multiple>
                                    @foreach(array_column($data['drugs'],'unit_number') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleFormControlInput1">Remedy Type</label>
                                <select class="" name="remedy_type[]" multiple>
                                    @foreach(array_column($data['drugs'],'remedy_type') as $val)
                                    <option value="{{$val}}">{{$val}}</option>
                                    @endforeach

                                </select>
                                <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                            </div>





                            <div class="form-group col-md-12">
                                <input class="btn btn-primary" type="submit" value="Subscribe" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!--row-->




@if(isset($filters) && count($filters) > 0 )
<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h3> {{($type ?? '')}} <small>Subscription List</small> </h3>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-md-12">

                        @php
                        $food = $filters->where('category','food');
                        $motor = $filters->where('category','motor');
                        $drugs = $filters->where('category','drugs');

                        @endphp

                        @if(count($motor))
                        <h2>Auto mobile / Trucks</h2>
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Filter</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($filters) && count($filters) > 0 )
                                @foreach($motor as $key=>$value)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$value->column_name}}</td>
                                    <td>{{$value->column_value}}</td>

                                    <td><a href="{{route('frontend.user.subscribe.edit',['type'=>'motor'])}}" class="btn btn-success">Edit</a></td>
                                </tr>

                                @endforeach
                                @endif


                            </tbody>
                        </table>
                        @endif

                        @if(count($food))
                        <h2>Food</h2>
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Filter</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($filters) && count($filters) > 0 )
                                @foreach($food as $key=>$value)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$value->column_name}}</td>
                                    <td>{{$value->column_value}}</td>

                                    <td><a href="{{route('frontend.user.subscribe.edit',['type'=>'food'])}}" class="btn btn-success">Edit</a></td>
                                </tr>

                                @endforeach
                                @endif


                            </tbody>
                        </table>
                        @endif

                        @if(count($drugs))
                        <h2>Medication</h2>
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Filter</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($filters) && count($filters) > 0 )
                                @foreach($drugs as $key=>$value)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$value->column_name}}</td>
                                    <td>{{$value->column_value}}</td>

                                    <td><a href="{{route('frontend.user.subscribe.edit',['type'=>'drugs'])}}" class="btn btn-success">Edit</a></td>
                                </tr>

                                @endforeach
                                @endif


                            </tbody>
                        </table>
                        @endif

                    </div>



                </div>
            </div>
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
@endif

<!--row-->
@endsection

@push('after-scripts')
<script>
    $("select[multiple]").multiselect();
</script>

@endpush
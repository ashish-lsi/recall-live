<style>
  <style>.recall-details-table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  .recall-details-table td,
  th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    font-family: arial, sans-serif;
  }

  .recall-details-table tr:nth-child(even) {
    background-color: #dddddd;
  }

  .recall-details-table tr td:first-child {
    width: 40%
  }

  .recall-details-table tr td:nth-child(2) {
    width: 60%
  }
</style>
</style>
<div id="mailsub" class="notification" align="center">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;">
    <tr>
      <td align="center" bgcolor="#eff3f8">


        <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
          <tr>
            <td>
              <!-- padding -->
              <div style="height: 50px; line-height: 50px; font-size: 10px;"> </div>
            </td>
          </tr>
          <!--header -->
          <tr>
            <td align="center" bgcolor="#fff">

              <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="left">
                    <div class="mob_center_bl" style="float: left; display: inline-block; width:100%;">
                      <table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;">
                        <tr>
                          <td align="center" valign="middle">

                            <table width="115" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td align="left" valign="top" class="mob_center">
                                  <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                    <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                      <img src="http://dev.recallalarms.com/public/img/Recall_alarms_logo.png" width="195" height="auto" alt="adopt logo" border="0" style="display: block;" /></font>
                                  </a>

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                  <td align="left">
                    <div class="mob_center_bl" style="float: left; display: inline-block; width:100%;">
                      <table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;">
                        <tr>
                          <td align="center" valign="middle">
                            <!-- padding -->
                            <div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
                            <table width="200" border="0" cellspacing="0" cellpadding="0">

                              <tr>
                                <td align="center" valign="top" class="mob_center" style="font-size:20px; font-family: Arial, Helvetica, sans-serif; color:#1C6CB0;"> HI {{$user->full_name}} </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top" style="font-family: Arial, Helvetica, sans-serif; padding:5px 0;"> Recall Report </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top" class="mob_center" style="font-family: Arial, Helvetica, sans-serif; "> {{date('M d,Y')}} </td>
                              </tr>



                            </table>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <!--header END-->
          <tr>
            <td align="center" bgcolor="#3b8acc">
              <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center">
                    <!-- padding -->
                    <div style="height: 0px; line-height: 20px; font-size: 10px;"> </div>
                    <h4 style="color:#fff; font-family: Arial, Helvetica, sans-serif; font-size:20px">Auto mobile / Trucks</h4>
                    <!-- <p style="color:#fff; font-family: Arial, Helvetica, sans-serif; font-size:14px"> Campaign No. 88E012001
                    </p> -->
</div>
<!-- padding -->
<div style=" font-size: 10px;"> </div>
</td>
</tr>



</table>
</tr>

<tr>
  <td align="center" bgcolor="#fff">
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <!-- padding -->
          <div style="height: 0px; line-height: 20px; font-size: 10px;"> </div>
          <h4 style="color:#E74C24; font-family: Arial, Helvetica, sans-serif; font-size:20px"> Recall Details</h4>

          </div>
          <!-- padding -->
          <div style="height: 0px; line-height: 20px; font-size: 10px;"> </div>
        </td>
      </tr>



    </table>
</tr>

<!--content 1 -->
@php 
$d = DateTime::createFromFormat('m/d/Y', $data['recall_date']);
@endphp 
<tr>
  <td align="center" bgcolor="#f5f5f5">

    <table class="recall-details-table" style="width:100%;">

      <tr>
        <td>Recall Date
        </td>
        <td>{{ $d ? $d->format('M d,Y') :''}}</td>

      </tr>
      <tr>
        <td>Failed Component </td>
        <td>{{$data['fail_comp']}} </td>

      </tr>
      <tr>
        <td>Recall Summary </td>
        <td>{{$data['summary']}}</td>

      </tr>

      <tr>
        <td>Make </td>
        <td>{{$data['make']}}</td>

      </tr>
      <tr>
        <td>Model  </td>
        <td>{{$data['model']}}</td>

      </tr>

      <tr>
        <td>Year</td>
        <td>{{$data['year']}}</td>

      </tr>

     
      <tr>
        <td>VIN Number</td>
        <td>{{$data['vin']}}</td>
      </tr>

      <tr>
        <td>Remedy Type </td>
        <td>{{$data['remedy']}}</td>
      </tr>

      <tr>
        <td>Phone</td>
        <td>{{$data['phone']}}</td>
      </tr>
      <tr>
        <td>Email</td>
        <td>{{$data['email']}}</td>
      </tr>

      <tr>
        <td>Text Messagin</td>
        <td>{{$data['txt_msg']}}</td>
      </tr>
      <tr>
        <td>Total  Recalled</td>
        <td>{{$data['t_recall']}}</td>
      </tr>
     

      <tr>
        <td>Incident  Date</td>
        <td>{{$data['incident_date']}}</td>
      </tr>

      <tr>
        <td>Incident Details (Crash/Fire/others)</td>
        <td>{{$data['incident_details']}}</td>
      </tr>
      

    </table>



  </td>
</tr>
<!--content 1 END-->



<tr>
  <td align="center" bgcolor="#fff">
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <!-- padding -->
          <div style="height: 0px; line-height: 20px; font-size: 10px;"> </div>
          <h5> <a href="#" target="_blank" style="color:#E74C24; font-family: Arial, Helvetica, sans-serif; font-size:20px"> For more information see your dashboard</a>
            <h5>

              </div>
              <!-- padding -->
              <div style="height: 0px; line-height: 20px; font-size: 10px;"> </div>
        </td>
      </tr>



    </table>
</tr>

<!--footer -->
<tr>
  <td class="iage_footer" align="center" bgcolor="#ffffff">
    <!-- padding -->
    <div style="height: 0px; line-height: 80px; font-size: 10px;"> </div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">


          <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
              © Copyright 2020 Recallalarms, Inc.. ALL Rights Reserved.
            </span></font>
        </td>
      </tr>
    </table>

    <!-- padding -->
    <div style="height: 0px; line-height: 30px; font-size: 10px;"> </div>
  </td>
</tr>
<!--footer END-->
<tr>
  <td>
    <!-- padding -->
    <div style="height: 0px; line-height: 80px; font-size: 10px;"> </div>
  </td>
</tr>
</table>
<!--[if gte mso 10]>
                </td></tr>
                </table>
                <![endif]-->

</td>
</tr>
</table>
</div>
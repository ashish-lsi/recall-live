<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">Auto mobile / Trucks</h2>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>

            <hr>

            <div class="row mb-4">
                <div class="col-md-12">


                    <form class="row" action="{{   route('frontend.user.subscribe.add',['type'=>'motor'])}}" method="POST">

                        @csrf

                        <div class="form-group col-md-6">
                            <label for="exampleFormControlInput1">Make</label>
                            <select class="" name="make[]" multiple>
                                @php
                                $filter_values = $filters->where('column_name','make')->first()->column_value ?? "";
                                $filter_values = explode(",",$filter_values);
                                @endphp
                                @foreach(array_column($data['motor'],'make') as $val)
                                <option value="{{$val}}" {{in_array($val,$filter_values) ? 'selected' : ''}}>{{$val}}</option>
                                @endforeach

                            </select>
                            <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleFormControlInput1">Model</label>
                            <select class="" name="model[]" multiple>
                                @php
                                $filter_values = $filters->where('column_name','model')->first()->column_value ?? "";
                                $filter_values = explode(",",$filter_values);
                                @endphp
                                @foreach(array_column($data['motor'],'model') as $val)
                                <option value="{{$val}}" {{in_array($val,$filter_values) ? 'selected' : ''}}>{{$val}}</option>
                                @endforeach

                            </select>
                            <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlInput1">Year</label>
                            <select class="" name="year[]" multiple>
                                @php
                                $filter_values = $filters->where('column_name','year')->first()->column_value ?? "";
                                $filter_values = explode(",",$filter_values);
                                @endphp
                                @foreach(array_column($data['motor'],'year') as $val)
                                <option value="{{$val}}" {{in_array($val,$filter_values) ? 'selected' : ''}}>{{$val}}</option>
                                @endforeach

                            </select>
                            <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleFormControlInput1">VIN Number</label>
                            <select class="" name="vin[]" multiple>
                                @php
                                $filter_values = $filters->where('column_name','vin')->first()->column_value ?? "";
                                $filter_values = explode(",",$filter_values);
                                @endphp
                                @foreach(array_column($data['motor'],'vin') as $val)
                                <option value="{{$val}}" {{in_array($val,$filter_values) ? 'selected' : ''}}>{{$val}}</option>
                                @endforeach

                            </select>
                            <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                        </div>



                        <div class="form-group col-md-12">
                            <input class="btn btn-primary" type="submit" value="Update" />
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
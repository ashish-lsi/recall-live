<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">Medication</h2>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>

            <hr>

            <div class="row mb-4">
                <div class="col-md-12">


                    <form class="row" action="{{   route('frontend.user.subscribe.add',['type'=>'drugs'])}}" method="POST">

                        @csrf

                        <div class="form-group col-md-6">
                            <label for="exampleFormControlInput1">Product Name</label>
                            <select class="" name="product_name[]" multiple>
                                @php
                                $filter_values = $filters->where('column_name','product_name')->first()->column_value ?? "";
                                $filter_values = explode(",",$filter_values);
                                @endphp
                                @foreach(array_column($data['drugs'],'product_name') as $val)
                                <option value="{{$val}}" {{in_array($val,$filter_values) ? 'selected' : ''}}>{{$val}}</option>
                                @endforeach

                            </select>
                            <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleFormControlInput1">Unit Number</label>
                            <select class="" name="unit_number[]" multiple>
                                @php
                                $filter_values = $filters->where('column_name','unit_number')->first()->column_value ?? "";
                                $filter_values = explode(",",$filter_values);
                                @endphp
                                @foreach(array_column($data['drugs'],'unit_number') as $val)
                                <option value="{{$val}}" {{in_array($val,$filter_values) ? 'selected' : ''}}>{{$val}}</option>
                                @endforeach

                            </select>
                            <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlInput1">Remedy Type</label>
                            <select class="" name="remedy_type[]" multiple>
                                @php
                                $filter_values = $filters->where('column_name','remedy_type')->first()->column_value ?? "";
                                $filter_values = explode(",",$filter_values);
                                @endphp
                                @foreach(array_column($data['drugs'],'remedy_type') as $val)
                                <option value="{{$val}}" {{in_array($val,$filter_values) ? 'selected' : ''}}>{{$val}}</option>
                                @endforeach

                            </select>
                            <!-- <input type="text" name="make" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"> -->
                        </div>

                       


                        <div class="form-group col-md-12">
                            <input class="btn btn-primary" type="submit" value="Update" />
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
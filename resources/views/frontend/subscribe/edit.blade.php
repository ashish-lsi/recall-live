@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@push('after-styles')
<style>
    .multiselect {
        border: 1px solid lightgray;

    }
</style>
@endpush
@section('content')

<!--row-->

<div class="row mb-4">
    @if($type == 'motor')
    @include('frontend.includes.fields.motor')
    @endif

    @if($type == 'food')
    @include('frontend.includes.fields.food')
    @endif

    @if($type == 'drugs')
    @include('frontend.includes.fields.drugs')
    @endif
</div>
<!--row-->




<!--row-->
@endsection

@push('after-scripts')
<script>
    $("select[multiple]").multiselect();
</script>

@endpush
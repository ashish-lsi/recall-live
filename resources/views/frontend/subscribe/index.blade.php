@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@push('after-styles')
<style>
.multiselect {
    border: 1px solid lightgray;

}
</style>
@endpush
@section('content')

<!--row-->


<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h3> {{$type}} <small>Subscription List</small> </h3>
            </div>
        </div>
    </div>
</div>


<!--row-->


<!--row-->
@endsection

@push('after-scripts')
<script>
    $("select[multiple]").multiselect();
</script>

@endpush
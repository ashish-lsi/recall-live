<style>
    .subscribe-btn{display:none;}
</style>

@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')
<div class="container">
<div class="row justify-content-center align-items-center">
    <div class="col col-sm-8 align-self-center">
        <div class="card card_form">
            <div class="card-header subscribe-us-form">
                <h4>Subscribe Us Form</h4>
            </div><!--card-header-->
            <form class="cmxform" id="signupForm" method="post" action="{{route('frontend.auth.register.post')}}">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="firstname">First Name <sup class="text-danger">*</sup></label>

                                <input type="text" name="first_name" id="firstname" placeholder="First Name" maxlength="191" required="required" class="form-control">
                            </div><!--col-->
                        </div><!--row-->

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="lastname">Last Name <sup class="text-danger">*</sup></label>

                                <input type="text" name="last_name" id="lastname" placeholder="Last Name" maxlength="191" required="required" class="form-control">
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="email">E-mail Address <sup class="text-danger">*</sup></label>

                                <input type="email" name="email" id="email" placeholder="E-mail Address" maxlength="191" required="required" class="form-control">
                            </div><!--form-group-->
                        </div><!--col-->

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="mobile">Mobile Number </label>
                                <input type="number" name="mobile" id="mobile" placeholder="mobile" class="form-control">
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="address">Address </label>

                                <input type="text" name="address" id="address" placeholder="Address" maxlength="191"  class="form-control">
                            </div><!--form-group-->
                        </div><!--col-->

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="city">City </label>

                                <input type="text" name="city" id="city" placeholder="City"  class="form-control">
                            </div><!--form-group-->
                        </div><!--col-->

                    </div><!--row-->


                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="address">State</label>

                                <input type="text" name="state" id="state" placeholder="State" maxlength="191"  class="form-control">
                            </div><!--form-group-->
                        </div><!--col-->

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="city">ZipCode </label>

                                <input type="number" name="zipcode" id="zipcode" placeholder="zipcode" maxlength="6"  class="form-control">
                            </div><!--form-group-->
                        </div><!--col-->

                    </div><!--row-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group Recall-type-ui">
                                <label for='Color'>Have you been affected by a recall before? <sup class="text-danger">*</sup></label>
                                <p class='container'>
                                    <label><input type='radio'  id="radioYes" name='recall_affected' value='Yes'  onclick="ShowTextbox()"/> Yes</label>
                                    <label><input type='radio' id="radioNo"  name='recall_affected' value='No' onclick="ShowTextbox()" checked=""/> No</label>
                                </p>
                                <label id="Color-error" class="error" for="Color" style='display: none'>Please select any value<br></label>

                                <div id="dvtext" style="display: none; margin-top:15px;">
                                    <label for="refood"> Type of Recall  </label> <br>
                                    <div class="recall-type">
                                        <label>  <input  type="checkbox" name="recall_type[]" value="food" class="checkbox"/> Food / beverage </label>
                                        <label> <input  type="checkbox"  name="recall_type[]" value="auto" class="checkbox"/> Automobile </label>
                                        <label><input  type="checkbox" name="recall_type[]" value="pharma" class="checkbox" /> Pharmaceutical </label>
                                        <label><input  type="checkbox" name="recall_type[]" value="childsafety" class="checkbox" /> Child Safety </label>
                                        <label for="type" id="re_food-error" class="error" style='display: none'>Please select any value.</label>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group Recall-type-ui">
                                <label for="food"> Type of recall you want to be notify <sup class="text-danger">*</sup></label>

                                <div class="recall-type">
                                    <label>  <input type="checkbox" name="recall_notify[]" value="food" class="checkbox" checked=""> Food / beverage </label>
                                    <label> <input type="checkbox" name="recall_notify[]" value="auto" class="checkbox"> Automobile </label>
                                    <label><input type="checkbox" name="recall_notify[]" value="pharma" class="checkbox"> Pharmaceutical </label>
                                    <label><input type="checkbox" name="recall_notify[]" value="childsafety" class="checkbox"> Child Safety </label>
                                    <label for="food" id="food-error" class="error" style='display: none'>Please select any value.</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                <button type="submit" class="btn btn-success btn-sm sub_btn">Subscribe us</button>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="text-center">
                                @include('frontend.auth.includes.socialite')
                            </div>
                        </div><!--/ .col -->
                    </div><!-- / .row -->
                </div><!-- card-body -->
            </form>
        </div><!-- card -->
    </div><!-- col-md-8 -->
</div><!-- row -->
</div><!-- container -->
@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush

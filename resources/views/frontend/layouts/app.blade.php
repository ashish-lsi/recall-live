<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
    @else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        @endlangrtl
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>@yield('title', app_name())</title>
            <meta name="description" content="@yield('meta_description', 'Laravel Boilerplate')">
            <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
            @yield('meta')
            <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
            <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />
            <link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}" />
            <link rel="stylesheet" href="{{asset('css/frontend.css')}}" />
            {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
            @stack('before-styles')

            <!-- Check if the language is set to RTL, so apply the RTL layouts -->
            <!-- Otherwise apply the normal LTR layouts -->
            {{ style(mix('css/frontend.css')) }}

            @stack('after-styles')
        </head>
        <body >
            @include('includes.partials.read-only')

            <div id="app">
                @include('includes.partials.logged-in-as')
                @include('frontend.includes.nav')

             
                    @include('includes.partials.messages')
                    @yield('content')
           
            </div><!-- #app -->

            <!-- Scripts -->
            @stack('before-scripts')
            {!! script(mix('js/manifest.js')) !!}
            {!! script(mix('js/vendor.js')) !!}
            {!! script(mix('js/frontend.js')) !!}
            {!! script('js/bootstrap-multiselect.js') !!}
            {!! script('js/jquery.validate.js') !!}
            {!! script('js/script.js') !!}
            @stack('after-scripts')

            @include('includes.partials.ga')
        </body>
    </html>

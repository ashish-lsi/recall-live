@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Manage Medication Imports'))
@section('content')


<div class="card" >
    <div class="col-sm-12">
        <br>
        @if ($message = Session::get('status'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
        @endif
        <div class="row">
            <div style="margin: 10px 10px 10px 15px">
                <a class="btn btn-primary" href="{{ route('admin.drugs.create') }}"> Import New Data</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>Search Medication Data
                    </div>
                    <div class="card-body">
                        <div class="container">
                            {{ Form::open(array('url' => route('admin.drugs.index'),'id'=>'filterForm','method'=>'GET')) }}
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>From Date </label>
                                    {!! Form::date('search[from_date]', $searchQuery['from_date'] ?? '', ['class' => 'form-control']) !!}
                                </div> 
                                <div class="col-sm-3">
                                    <label>To Date </label>
                                    {!! Form::date('search[to_date]', $searchQuery['to_date'] ?? '', ['class' => 'form-control']) !!}
                                </div> 
                                <div class="col-sm-3">
                                    <label>Product name</label>
                                    {!! Form::text('search[product_name]', $searchQuery['product_name'] ?? '', ['class' => 'form-control', 'placeholder' => 'Product name...']) !!}
                                </div> 
                                <div class="col-sm-3">
                                    <label>Product description</label>
                                    {!! Form::text('search[product_description]', $searchQuery['product_description'] ?? '', ['class' => 'form-control', 'placeholder' => 'Product description...']) !!}
                                </div> 
                                <div class="col-sm-3">
                                    <label>Unit number</label>
                                    {!! Form::text('search[unit_number]', $searchQuery['unit_number'] ?? '', ['class' => 'form-control', 'placeholder' => 'Unit number...']) !!}
                                </div> 
                                <div class="col-sm-3">
                                    <label>Remedy type</label>
                                    {!! Form::text('search[remedy_type]', $searchQuery['remedy_type'] ?? '', ['class' => 'form-control', 'placeholder' => 'Remedy type...']) !!}
                                </div> 
                            </div>
                            <br>
                            <div class="row">
                                <span class="input-group-btn">
                                    &nbsp;&nbsp;
                                    <input type="submit" class="btn btn-success" value="Search">
                                    <a class="btn btn-warning" href="{{route('admin.drugs.index')}}">Reset</a>
                                </span>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Medication Data</div>
                    <div class="card-body">
                        <div style="float: right">Total records <b>: {{$results->total()}}</b></div>
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Recall Date</th>
                                    <th>Product name</th>
                                    <th>Product description</th>
                                    <th>Unit number</th>
                                    <th>Remedy type</th>
                                    <th>Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $result)
                                @php
                                $d = DateTime::createFromFormat('m/d/Y', $result->recall_date);
                                @endphp
                                <tr>
                                    <td>{{$d ? $d->format('M d,Y') :''}}</td>
                                    <td>{{$result->product_name}}</td>
                                    <td>{{$result->product_description}}</td>
                                    <td>{{$result->unit_number}}</td>
                                    <td>{{$result->remedy_type}}</td>
                                    <td>{{$result->created_at->format('M d, Y')}}</td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            <?php
                            $searchQuery1['search'] = $searchQuery;
                            ?>
                            {!! $results->appends($searchQuery1)->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
@endsection
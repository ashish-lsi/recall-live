@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Import Medication Data'))
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Import Medication Data
            </div>
            <div class="card-body">
                <div style="float: left">
                    <a href="{{asset('storage/sample/sample_drugs.csv')}}" target="_blank" alt="Download Sample File">
                        Download Sample 
                    </a>
                    <ol>
                        <li>Download the sample file</li>
                        <li>Paste your data into that according to column</li>
                        <li>Upload the file below</li>
                    </ol>
                </div>
                <div class="table-responsive">
                    <form action="{{route('admin.drugs.import')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <table class="table table-hover">
                            <tr>
                                <th>Select File</th>
                                <td>
                                    <input type="file" name="file" required>
                                </td>
                            </tr>

                            <tr>
                                <th>Status</th>
                                <td>
                                    <div id="bar"></div>
                                    <div id="log"></div>
                                </td>
                            </tr>
                            <td colspan="2" align='center'>
                                <button type="submit" class="btn btn-success">Submit</button>
                                <button type="Reset" class="btn btn-danger">Reset</button>
                                <a class="btn btn-warning" href="{{ route('admin.drugs.index') }}">Cancel</a>
                            </td>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    body {
        margin:10px;   
    }
    #bar {
        width:0px;
        height:25px;
        border:1px solid black;
        background-color:yellow;
        border-radius:3px;
    }
</style>
@endsection

@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var maxWidth = 400;
        var duration = 20000;
        var $log = $('#log');
        var $start = $('#start');
        var $stop = $('#stop');
        var timer;

        $('form').on('submit', function () {
            var $bar = $('#bar');
            Horloge(maxWidth);
            timer = setInterval('Horloge(' + maxWidth + ')', 100);

            $bar.animate({
                width: maxWidth
            }, duration, function () {
                $(this).css('background-color', 'green');
                $start.attr('disabled', true);
                $stop.attr('disabled', true);
                $log.html('100 %');
                clearInterval(timer);
            });
        });
    });

    function Horloge(maxWidth) {
        var w = $('#bar').width();
        var percent = parseInt((w * 100) / maxWidth);
        $('#log').html(percent + ' %');
    }
</script>
@endpush
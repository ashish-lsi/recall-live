@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Add Category'))
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Import Motors Data
            </div>
            <div class="card-body">
                <div style="float: left">
                    <a class="btn btn-success" href="{{route('admin.recall.alert')}}"  alt="Send Recall Alert">
                        Send Recall Alert
                    </a>
                </div>
                <br>
                <br>
                <br>

                <div class="table-responsive">
                   <?php

                    use App\AlertHistory;

                    $alerts = AlertHistory::join('users','users.id','=','alert_history.user_id')
                                            ->orderBy('alert_history.id','DESC')
                                            ->paginate(20);

                   ?> 
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>User</th>
                                    <th>Category</th>
                                    <th>Recall ID</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($alerts as $key=> $alert)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$alert->email}}</td>
                                    <td>{{$alert->category}}</td>
                                    <td>{{$alert->alert_id}}</td>
                                    <td>{{$alert->created_at}}</td>

                                </tr>

                                @endforeach

                            </tbody>
                           
                        </table>
                </div>
               
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    body {
        margin:10px;   
    }
    #bar {
        width:0px;
        height:25px;
        border:1px solid black;
        background-color:yellow;
        border-radius:3px;
    }
</style>
@endsection

@push('after-scripts')

@endpush
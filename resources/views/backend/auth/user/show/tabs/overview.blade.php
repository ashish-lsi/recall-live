<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.avatar')</th>
                <td><img src="{{ $user->picture }}" class="user-profile-image" /></td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.table.first_name')</th>
                <td>{{ $user->first_name }}</td>
            </tr>
            
            <tr>
                <th>@lang('labels.backend.access.users.table.last_name')</th>
                <td>{{ $user->last_name }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.table.email'))</th>
                <td>{{ $user->email }}</td>
            </tr>

            <tr>
                <th>Mobile</th>
                <td>{{ $user->mobile ?? '-' }}</td>
            </tr>

            <tr>
                <th>Address</th>
                <td>{{ $user->address ?? '-' }}</td>
            </tr>

            <tr>
                <th>City</th>
                <td>{{ $user->city ?? '-' }}</td>
            </tr>

            <tr>
                <th>State</th>
                <td>{{ $user->state ?? '-' }}</td>
            </tr>

            <tr>
                <th>Zipcode</th>
                <td>{{ $user->zipcode ?? '-' }}</td>
            </tr>

            <tr>
                <th>Have you been affected <br> by a recall before?</th>
                <td>{{ $user->recall_affected ?? '-' }}</td>
            </tr>

            <tr>
                <th>Type of Recall</th>
                <td>{{ $user->recall_type ?? '-' }}</td>
            </tr>

            <tr>
                <th>Type of recall you want to be notify</th>
                <td>{{ $user->recall_notify ?? '-' }}</td>
            </tr>
            
        </table>
    </div>
</div><!--table-responsive-->

@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Manage Motor Imports'))
@section('content')


<div class="card">
    <div class="col-sm-12">
        <br>
        @if ($message = Session::get('status'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
        @endif
        <div class="row">
            <div style="margin: 10px 10px 10px 15px">
                <a class="btn btn-primary" href="{{ route('admin.motor.create') }}"> Import New Data</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>Search Auto mobile / Trucks Data
                    </div>
                    <div class="card-body">
                        <div class="container">
                            {{ Form::open(array('url' => route('admin.motor.index'),'id'=>'filterForm','method'=>'GET')) }}
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>From Date </label>
                                    {!! Form::date('search[from_date]', $searchQuery['from_date'] ?? '', ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-sm-3">
                                    <label>To Date </label>
                                    {!! Form::date('search[to_date]', $searchQuery['to_date'] ?? '', ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-sm-3">
                                    <label>Make</label>
                                    {!! Form::text('search[make]', $searchQuery['make'] ?? '', ['class' => 'form-control', 'placeholder' => 'Make...']) !!}
                                </div>
                                <div class="col-sm-3">
                                    <label>Model</label>
                                    {!! Form::text('search[model]', $searchQuery['model'] ?? '', ['class' => 'form-control', 'placeholder' => 'Model...']) !!}
                                </div>
                                <div class="col-sm-3">
                                    <label>Year</label>
                                    {!! Form::text('search[year]', $searchQuery['year'] ?? '', ['class' => 'form-control', 'placeholder' => 'Year...']) !!}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <span class="input-group-btn">
                                    &nbsp;&nbsp;
                                    <input type="submit" class="btn btn-success" value="Search">
                                    <a class="btn btn-warning" href="{{route('admin.motor.index')}}">Reset</a>
                                </span>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Auto mobile / Trucks Data</div>
                    <div class="card-body">
                        <div style="float: right">Total records <b>: {{$results->total()}}</b></div>
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Recall Date</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Year</th>
                                    <th>Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $result)
                                @php
                                $d = DateTime::createFromFormat('m/d/Y', $result->recall_date);
                                @endphp
                                <tr>
                                    <td>{{$d ? $d->format('M d,Y') :''}}</td>
                                    <td>{{$result->make}}</td>
                                    <td>{{$result->model}}</td>
                                    <td>{{$result->year}}</td>
                                    <td>{{ $result->created_at->format('M d, Y') }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            <?php
                            $searchQuery1['search'] = $searchQuery;
                            ?>
                            {!! $results->appends($searchQuery1)->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
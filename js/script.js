$(document).ready(function () {
    // validate the comment form when it is submitted
    //$("#commentForm").validate();

    // validate signup form on keyup and submit
    $("#signupForm").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            username: {
                required: true,
                minlength: 2
            },
            Color: {required: true},
            food: {required: true},
            refood: {required: true},
            mobile: {
                required: true,
                minlength: 10
            },
            password: {
                required: true,
                minlength: 5
            },
            password_confirmation: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            topic: {
                required: "#newsletter:checked",
                minlength: 2
            },
            agree: "required"
        },
        messages: {
            firstname: "Please enter your firstname",
            lastname: "Please enter your lastname",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            Color:
                    {
                        required: "This field is required.<br/>"
                    },
            food:
                    {
                        required: "This field is required.<br/>"
                    },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
            mobile: "Please enter a valid Mobile Number",
            agree: "Please accept our policy",
            topic: "Please select at least 2 topics",
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    //code to hide topic selection, disable for demo
    var newsletter = $("#newsletter");
    // newsletter topics are optional, hide at first
    var inital = newsletter.is(":checked");
    var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
    var topicInputs = topics.find("input").attr("disabled", !inital);
    // show when newsletter is checked
    newsletter.click(function () {
        topics[this.checked ? "removeClass" : "addClass"]("gray");
        topicInputs.attr("disabled", !this.checked);
    });
});


function ShowTextbox() {
    var radioYes = document.getElementById("radioYes");
    var dvtext = document.getElementById("dvtext");
    dvtext.style.display = radioYes.checked ? "block" : "none";
}
	
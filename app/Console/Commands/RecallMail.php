<?php

namespace App\Console\Commands;

use App\AlertHistory;
use App\Category;
use App\Mail\RecallAlertMail;
use App\Models\Auth\User;
use DB;
use Illuminate\Console\Command;
use Log;
use Mail;

class RecallMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alerts:recall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $category = Category::all();

        $users = User::with('filters')
        //->where('id', 9)
        ->get();
        foreach ($users as $user) {

            if ($user->roles_label == "User") {

                $email = $user->email;

                foreach ($category as $cat) {
                    $filter = $user->filters()->where('category', $cat->category)->get()->toArray();
                    $query = "Select ";
                    // $query .= implode(',',array_column($filter,"column_name") );
                    $query .= " * ";
                    $query .= " FROM " . $cat->category;
                    // $query .= " join alert_history ON  ".$cat->category.".id  != alert_history.alert_id  AND ";
                    //$query .= " alert_history.category = '". $cat->category . "' ";
                    $query .= " WHERE 1=1 ";

                    foreach ($filter as $f) {

                        $query .= " AND " . $f['column_name'] . " IN ( '" . implode("','", explode(",", $f['column_value'])) . "') ";
                    }
                    $query .= " AND  id not IN (select alert_id from alert_history where category = '$cat->category' and user_id = '$user->id' ) ;";
                    Log::info($query);
                    $alert =  DB::select($query);
                    foreach ($alert as $a) {
                        // dd($a);
                        $alert = new AlertHistory();
                        $alert->category = $cat->category;
                        $alert->user_id = $user->id;
                        $alert->alert_id = $a->id;
                        $alert->save();

                        $recallMail = new RecallAlertMail((array) $a, $cat->category, $user);
                        Mail::to($email)->send($recallMail);
                        sleep(1);
                    }
                    // print_r($query);
                    //dd($alert);
                    //dd($filter->toArray());
                    // foreach($user->filters() as $filter)
                    //dd($query);
                }
            }
        }
        //dd($users->filters());

    }
}

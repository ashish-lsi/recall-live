<?php

use App\Helpers\General\HtmlHelper;

if (!function_exists('style')) {
    /**
     * @param       $url
     * @param array $attributes
     * @param null  $secure
     *
     * @return mixed
     */
    function style($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->style($url, $attributes, $secure);
    }
}

if (!function_exists('script')) {
    /**
     * @param       $url
     * @param array $attributes
     * @param null  $secure
     *
     * @return mixed
     */
    function script($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->script($url, $attributes, $secure);
    }
}

if (!function_exists('render_html')) {
    /**
     * @param       $url
     * @param array $attributes
     * @param null  $secure
     *
     * @return mixed
     */
    function render_html($type, $name, $input_values,$edit_values , $attributes)
    {   $html = "";
        if($type == "Select"){
            $html .= "<select class='form-control' name='". (strpos($attributes,'multiple') !== false ? $name."[]" : $name ) ."' $attributes >";
            $values = explode(",",$input_values);
            $edit = explode(",",$edit_values);
            foreach($values as $value) {
                $html .= "<option value='$value' ".(in_array($value,$edit) ? 'selected' : '')."> $value </option>";

            }
            $html .= "</select>";

        } elseif ($type === "Date") {
            $html .= "<input type='date' class='form-control' value='$edit_values' name='$name' $attributes>";           
           
        }
        elseif ($type === "Text") {
            $html .= "<input type='text' class='form-control' value='$edit_values' name='$name' $attributes>";           
           
        }
        return $html;
    }
}

if (!function_exists('form_cancel')) {
    /**
     * @param        $cancel_to
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_cancel($cancel_to, $title, $classes = 'btn btn-danger btn-sm')
    {
        return resolve(HtmlHelper::class)->formCancel($cancel_to, $title, $classes);
    }
}

if (!function_exists('form_submit')) {
    /**
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_submit($title, $classes = 'btn btn-success btn-sm pull-right')
    {
        return resolve(HtmlHelper::class)->formSubmit($title, $classes);
    }
}

if (!function_exists('active_class')) {
    /**
     * Get the active class if the condition is not falsy.
     *
     * @param        $condition
     * @param string $activeClass
     * @param string $inactiveClass
     *
     * @return string
     */
    function active_class($condition, $activeClass = 'active', $inactiveClass = '')
    {
        return $condition ? $activeClass : $inactiveClass;
    }
}

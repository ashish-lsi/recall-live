<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Motor;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/**
 * Class DashboardController.
 */
class MotorController extends Controller {

    public function __construct() {
        ini_set('memory_limit', '300M');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index() {

        $model = new Motor();
        $searchQuery = $_GET['search'] ?? [];

        $results = $model->search_details($searchQuery);

        return view('backend.motor.index', compact('results', 'searchQuery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.motor.create');
    }

    public function import(Request $request) {
        request()->validate([
            'file' => 'required|mimes:csv,txt'
        ]);

        //get file from upload
        $path = request()->file('file')->getRealPath();
        $path = str_replace("\\", "/", $path);
        $user = auth()->user()->id;

        //Motor::truncate();
        $query = "LOAD DATA LOCAL INFILE '$path'
                    INTO TABLE motor FIELDS TERMINATED BY ','
                    ENCLOSED BY '\"' 
                    LINES TERMINATED BY '\r\n'
                    IGNORE 1 LINES (recall_date, category, notes, campaign_no, nhtsa_campaign_no, fail_comp, summary, make, model, year, vin, remedy, action_taken, action_date, firm_contact_info, mfg_contact_info, firm_resp_contact_info, retail_contact_info, press, mailing, phone, fax, email, txt_msg, social_media, t_recall, t_return, cust_notify, cust_not_respond, incident_details, incident_date, no_incident, no_injuries, no_death, med_report, term_sum, term_date, contact_info, prod_images, support_docs) SET id = NULL, created_by = $user;";

        $pdo = DB::connection()->getPdo();
        $recordsCount = $pdo->exec($query);

        session()->flash('status', "$recordsCount Records Uploaded!!!!");

        return redirect("admin/motor");
    }

}

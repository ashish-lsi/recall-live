<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Drugs;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/**
 * Class DashboardController.
 */
class DrugsController extends Controller {

    public function __construct() {
        ini_set('memory_limit', '300M');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index() {

        $model = new Drugs();
        $searchQuery = $_GET['search'] ?? [];

        $results = $model->search_details($searchQuery);
        return view('backend.drugs.index', compact('results', 'searchQuery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.drugs.create');
    }

    public function import(Request $request) {
        request()->validate([
            'file' => 'required|mimes:csv,txt'
        ]);

        //get file from upload
        $path = request()->file('file')->getRealPath();
        $path = str_replace("\\", "/", $path);
        $user = auth()->user()->id;

        //Motor::truncate();

        $query = "LOAD DATA LOCAL INFILE '$path'
                    INTO TABLE drugs FIELDS TERMINATED BY ','
                    ENCLOSED BY '\"' 
                    LINES TERMINATED BY '\r\n'
                    IGNORE 1 LINES (recall_date,fda_category,notes,destruction_method,recall_summary,product_name,product_number,product_description,route_of_administration,prescription,strength,unit_number,expiration,upc_codes,nda_number,ndc_number,remedy_type,action_taken,action_date,recall_firm_contact_info,manufacturer_contact_info,firm_responsible_contact_info,press_release,mailing_letter,phone,fax,email,text_messaging,social_media,total_recalled,total_returned,customer_noified,customer_not_responding,hazard_details,incident_details,no_of_incidents,no_of_injuries,no_of_deaths,medical_assessment_report,termination_summary,termination_date,product_images,product_label,supporting_documents,other) SET id = NULL, created_by = $user;";

        $pdo = DB::connection()->getPdo();
        $recordsCount = $pdo->exec($query);

        session()->flash('status', "$recordsCount Records Uploaded!!!!");

        return redirect("admin/drugs");
    }

}

<?php

namespace App\Http\Controllers\Frontend;

use App\Frontend\Drugs;
use App\Frontend\SubscripionEntity;
use App\Http\Controllers\Controller;
use App\Frontend\Filters;
use App\Frontend\Food;
use App\Frontend\Motor;
use DB;
use Illuminate\Http\Request;

class SubscribeController extends Controller {

    //

    public function view(Request $request, $type, $edit = false) {

        $subscriptionEntity = SubscripionEntity::where('category', $type)->orderby('sort', 'ASC')->get();
        $table = "";
        $values = [];
        if ($type == "food")
            $table = "food";
        elseif ($type == "motor")
            $table = "motor";
        elseif ($type == "drugs")
            $table = "drugs";


        $column_name = DB::select("select ISC.COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS as `ISC`
             join recall.subscription_entity `RSE` on RSE.column_name = ISC.column_name
             WHERE TABLE_NAME = '$table' and RSE.category = '$table'", [1]);
        // dd($field_values);
        $fields = implode(",", array_column($column_name, 'COLUMN_NAME'));
        ;
        if ($fields != "")
            $values = DB::select("select $fields from $table  group by $fields");

        if ($edit) {
            $filters = Filters::where(['category' => $type, 'user_id' => auth()->user()->id])->get();
            return view('frontend.index', compact('type', 'subscriptionEntity', 'edit', 'values', 'filters'));
        }


        return view('frontend.index', compact('type', 'subscriptionEntity', 'values'));

        if ($fields != "")
            $values = DB::select("select $fields from $table  group by $fields");

        return view('frontend.subscribe.index', compact('type', 'subscriptionEntity', 'values'));
    }

    public function viewSubscription(Request $request) {
        if (auth()->user()) {
            $data = [];
            $motor = Motor::select('make', 'model', 'year', 'vin')->groupBy('make', 'model', 'year', 'vin')->take(10)->get()->toArray();
            $food = Food::select('product_name', 'unit_number', 'remedy_type')->groupBy('product_name', 'unit_number', 'remedy_type')->take(10)->get()->toArray();
            $drugs = Drugs::select('product_name', 'unit_number', 'remedy_type')->groupBy('product_name', 'unit_number', 'remedy_type')->take(10)->get()->toArray();
            $data['motor'] = $motor;
            $data['food'] = $food;
            $data['drugs'] = $drugs;



            $user_id = auth()->user()->id;
            $filters = Filters::where('user_id', $user_id)->get();
            //dd($filters);
            return view('frontend.index', compact('filters', 'data'));
        }
        return view('frontend.index');
    }

    public function add(Request $request, $type) {

        Filters::where('user_id', auth()->user()->id)->where('category', $type)->delete();

        $input = $request->all();
        foreach ($input as $key => $value) {
            if ($key != "_token") {
                // dd($key);
                $filters = new Filters();
                $filters->category = $type;
                $filters->user_id = auth()->user()->id;
                $filters->column_name = $key;
                $filters->column_value = is_array($value) ? implode(",", $value) : $value;
                $filters->save();
            }
        }
        return redirect('/')->with(["result" => "success", "msg" => "Records Updated Successfully."]);
    }

    public function edit(Request $request, $type) {
        // $subscriptionEntity = SubscripionEntity::where('category', $type)->orderby('sort','ASC')->get();
        // $filters = Filters::where(['category'=>$type,'user_id'=>auth()->user()->id])->get();
        // dd($filters);
        return $this->view($request, $type, true);
        //    $edit=true;
        //     return view('frontend.index', compact('type', 'subscriptionEntity','edit','filters'));
    }

    public function editSubscription(Request $request, $type) {

        $data = [];
        $motor = Motor::select('make', 'model', 'year', 'vin')->groupBy('make', 'model', 'year', 'vin')->take(10)->get()->toArray();
        $food = Food::select('product_name', 'unit_number', 'remedy_type')->groupBy('product_name', 'unit_number', 'remedy_type')->take(10)->get()->toArray();
        $drugs = Drugs::select('product_name', 'unit_number', 'remedy_type')->groupBy('product_name', 'unit_number', 'remedy_type')->take(10)->get()->toArray();
        $data['motor'] = $motor;
        $data['food'] = $food;
        $data['drugs'] = $drugs;

        $user_id = auth()->user()->id;
        $filters = Filters::where('user_id', $user_id)->where('category', $type)->get();
        //dd($filters->where('column_name','make')->first()->column_value);

        return view('frontend.subscribe.edit', compact('type', 'data', 'filters'));
    }

    public function update(Request $request, $type) {
        Filters::where('user_id', auth()->user()->id)->where('category', $type)->delete();
        $input = $request->all();
        foreach ($input as $key => $value) {
            if ($key != "_token") {
                // dd($key);
                $filters = new Filters();
                $filters->category = $type;
                $filters->user_id = auth()->user()->id;
                $filters->column_name = $key;
                $filters->column_value = is_array($value) ? implode(",", $value) : $value;
                $filters->save();
            }
        }
        return back()->with(["result" => "success", "msg" => "Records Updated Successfully."]);
    }

}

<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Repositories\Frontend\Auth\UserRepository;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Auth\User;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller {

    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectPath() {
        return route(home_route());
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm() {
        abort_unless(config('access.registration'), 404);

        return view('frontend.auth.register');
    }

    /**
     * @param RegisterRequest $request
     *
     * @throws \Throwable
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request) {
        abort_unless(config('access.registration'), 404);

        $model = new User();

//        request()->validate([
//            'first_name' => 'required|min:3|max:50',
//            'last_name' => 'required|min:2|max:50',
//            'email' => 'required|email|unique:users',
//            'mobile' => 'required|numeric|unique:users',
//            'address' => 'required|min:3|max:150',
//            'city' => 'required|min:3|max:50',
//            'state' => 'required|max:50',
//            'zipcode' => 'required|numeric',
//            'recall_affected' => 'required',
//            'recall_notify' => 'required',
//        ]);

        $model::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zipcode' => $request->zipcode,
            'recall_affected' => $request->recall_affected,
            'recall_type' => isset($request->recall_type) && count($request->recall_type) > 0 ? implode(',', $request->recall_type) : '',
            'recall_notify' => isset($request->recall_notify) && count($request->recall_notify) > 0 ? implode(',', $request->recall_notify) : '',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'active' => true,
            'password' => '123456',
            // If users require approval or needs to confirm email
            'confirmed' => !(config('access.users.requires_approval') || config('access.users.confirm_email')),
        ]);

        return redirect('/')->withFlashSuccess('You are subcribed successfully');

        $user = $this->userRepository->create($request->only('first_name', 'last_name', 'email', 'mobile', 'password', 'city', 'state', 'zipcode', 'recall_affected', 'recall_notify'));

        // If the user must confirm their email or their account requires approval,
        // create the account but don't log them in.
        if (config('access.users.confirm_email') || config('access.users.requires_approval')) {
            event(new UserRegistered($user));

            return redirect($this->redirectPath())->withFlashSuccess(
                            config('access.users.requires_approval') ?
                            __('exceptions.frontend.auth.confirmation.created_pending') :
                            __('exceptions.frontend.auth.confirmation.created_confirm')
            );
        }

        auth()->login($user);

        event(new UserRegistered($user));

        return redirect('/')->withFlashSuccess('You are subcribed successfully');
    }

}

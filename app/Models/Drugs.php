<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
/**
 * Class PasswordHistory.
 */
class Drugs extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'drugs';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['recall_date','fda_category','notes','destruction_method','recall_summary','product_name','product_number','product_description','route_of_administration','prescription','strength','unit_number','expiration','upc_codes','nda_number','ndc_number','remedy_type','action_taken','action_date','recall_firm_contact_info','manufacturer_contact_info','firm_responsible_contact_info','press_release','mailing_letter','phone','fax','email','text_messaging','social_media','total_recalled','total_returned','customer_noified','customer_not_responding','hazaed_details','incident_details','no_of_incidents','no_of_injuries','no_of_deaths','medical_assessment_report','termination_summary','termination_date','product_images','product_label','supporting_documents','other','created_at','updated_at'];

    
    public function search_details($filter) {

        $user = auth()->user()->id;
        $results = Drugs::latest()->where('created_by', $user);
        
        //Search starts here
        if (is_array($filter)) {
            if (isset($filter['unit_number']) && $filter['unit_number'] != "") {
                $results->where('unit_number', 'LIKE', "%".$filter['unit_number']."%");
            }
            if (isset($filter['product_name']) && $filter['product_name'] != "") {
                $results->where('product_name', 'LIKE', "%".$filter['product_name']."%");
            }
            if (isset($filter['product_description']) && $filter['product_description'] != "") {
                $results->where('product_description', 'LIKE', "%".$filter['product_description']."%");
            }
            if (isset($filter['remedy_type']) && $filter['remedy_type'] != "") {
                $results->where('remedy_type', 'LIKE', "%".$filter['remedy_type']."%");
            }
            // Search for a post based on their dates.
            if (isset($filter['from_date']) && $filter['from_date'] != "") {
                $q = date('Y-m-d', strtotime($filter['from_date']));
                $results->where(DB::raw("DATE_FORMAT(STR_TO_DATE(`recall_date`,'%d-%m-%Y'), '%Y-%m-%d')"), '>=', $q);
            }

            // Search for a post based on their dates.
            if (isset($filter['to_date']) && $filter['to_date'] != "") {
                $q = date('Y-m-d', strtotime($filter['to_date']));
                $results->where(DB::raw("DATE_FORMAT(STR_TO_DATE(`recall_date`,'%d-%m-%Y'), '%Y-%m-%d')"), '<=', $q);
            }
        }

        return $results->paginate(50);        
    }
}

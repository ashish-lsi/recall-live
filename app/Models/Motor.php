<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
/**
 * Class PasswordHistory.
 */
class Motor extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'motor';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['recall_date', 'category', 'notes', 'campaign_no', 'nhtsa_campaign_no', 'fail_comp', 'summary', 'make', 'model', 'year', 'vin', 'remedy', 'action_taken', 'action_date', 'firm_contact_info', 'mfg_contact_info', 'firm_resp_contact_info', 'retail_contact_info', 'press', 'mailing', 'phone', 'fax', 'email', 'txt_msg', 'social_media', 't_recall', 't_return', 'cust_notify', 'cust_not_respond', 'incident_details', 'incident_date', 'no_incident', 'no_injuries', 'no_death', 'med_report', 'term_sum', 'term_date', 'contact_info', 'prod_images', 'support_docs', 'created_on'];

    public function search_details($filter) {

        $user = auth()->user()->id;
        $results = Motor::latest()->where('created_by', $user);
        
        //Search starts here
        if (is_array($filter)) {
            if (isset($filter['make']) && $filter['make'] != "") {
                $results->where('make', $filter['make']);
            }
            if (isset($filter['model']) && $filter['model'] != "") {
                $results->where('model', $filter['model']);
            }
            if (isset($filter['year']) && $filter['year'] != "") {
                $results->where('year', $filter['year']);
            }
            // Search for a post based on their dates.
            if (isset($filter['from_date']) && $filter['from_date'] != "") {
                $q = date('Y-m-d', strtotime($filter['from_date']));
                $results->where(DB::raw("DATE_FORMAT(STR_TO_DATE(`recall_date`,'%d-%m-%Y'), '%Y-%m-%d')"), '>=', $q);
            }

            // Search for a post based on their dates.
            if (isset($filter['to_date']) && $filter['to_date'] != "") {
                $q = date('Y-m-d', strtotime($filter['to_date']));
                $results->where(DB::raw("DATE_FORMAT(STR_TO_DATE(`recall_date`,'%d-%m-%Y'), '%Y-%m-%d')"), '<=', $q);
            }
        }

        return $results->paginate(50);        
    }

}

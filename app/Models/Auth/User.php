<?php

namespace App\Models\Auth;

use App\Frontend\Filters;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use App\Models\Auth\Traits\Method\UserMethod;
use App\Models\Auth\Traits\Relationship\UserRelationship;
use App\Models\Auth\Traits\Scope\UserScope;

/**
 * Class User.
 */
class User extends BaseUser {

    use UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'mobile', 'address', 'city', 'state', 'zipcode', 'recall_affected', 'recall_type', 'recall_notify'
    ];

    public function filters() {
        return $this->hasMany(Filters::class, 'user_id');
    }

}

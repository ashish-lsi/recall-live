<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecallAlertMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data,$template,$user;
    public function __construct($a,$template,$user )
    {
        //
        $this->data = $a;
        $this->template = $template;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('frontend.mail.recallAlert_'.$this->template,['data'=>$this->data,'user'=>$this->user])
        ->subject(__("  Recall Alert", ['app_name' => app_name()]))
        ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
